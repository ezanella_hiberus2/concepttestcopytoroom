package com.example.myapplication.History

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "history")
data class History(
    @PrimaryKey
    @ColumnInfo(name = "wordId") val wordId: Int? = null,
    @ColumnInfo(name = "wordfrom") val wordfrom: String,
    @ColumnInfo(name = "wordto") val wordto: String,
    @ColumnInfo(name = "favflag") val favflag: String
)

//    CREATE TABLE "history" (
//    `wordId` INTEGER,
//    `wordfrom` TEXT NOT NULL,
//    `wordto` TEXT NOT NULL,
//    `favflag` TEXT NOT NULL,
//    PRIMARY KEY(`wordId`))