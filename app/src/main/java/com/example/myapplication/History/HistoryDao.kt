package com.example.myapplication.History

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface HistoryDao {

    @Insert
    fun insertAll(usuario: History)

    @Delete
    fun delete(usuario: History)

    @Query("SELECT * FROM history")
    fun getAllHistory() : List<History>

}