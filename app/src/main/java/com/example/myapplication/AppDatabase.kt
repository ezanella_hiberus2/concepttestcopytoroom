package com.example.myapplication

import android.util.Log
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.myapplication.Configuracion.Configuracion
import com.example.myapplication.Configuracion.ConfiguracionDao
import com.example.myapplication.History.History
import com.example.myapplication.History.HistoryDao
import com.example.myapplication.SisPerfil.Perfil
import com.example.myapplication.SisPerfil.PerfilDao


@Database(
    entities = [History::class, Perfil::class, Configuracion::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun historyDao(): HistoryDao

    abstract fun perfilDao(): PerfilDao

    abstract fun configuracionDao(): ConfiguracionDao

    companion object {
        @JvmField
        val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                Log.e("Create Table", "into")

                database.execSQL(
                    ("CREATE TABLE IF NOT EXISTS NEW_SIS_PERFIL (idPerfil INTEGER NOT NULL, " +
                            "nombre TEXT NOT NULL, descripcion TEXT NOT NULL, PRIMARY KEY(idPerfil))").trimIndent())

                database.execSQL(
                    ("INSERT INTO NEW_SIS_PERFIL (idPerfil, nombre, descripcion) " +
                            "SELECT idPerfil, nombre, descripcion FROM SIS_PERFIL").trimIndent())

                database.execSQL("DROP TABLE SIS_PERFIL")
                database.execSQL("ALTER TABLE NEW_SIS_PERFIL RENAME TO SIS_PERFIL")
            }
        }
//
//        val MIGRATION_2_3: Migration = object : Migration(1, 2) {
//            override fun migrate(database: SupportSQLiteDatabase) {
//                Log.e("Create Table", "into")
//                database.execSQL(
//                    "INSERT INTO SIS_PERFIL (idPerfil, nombre, descripcion) SELECT idPerfil, nombre, descripcion FROM SIS_PERFIL")
//            }
//        }
    }
}