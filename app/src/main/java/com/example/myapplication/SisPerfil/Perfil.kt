package com.example.myapplication.SisPerfil

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "SIS_PERFIL")
data class Perfil(
    @PrimaryKey
    @ColumnInfo(name = "idPerfil") val idPerfil: Int,
    val nombre: String,
    val descripcion: String
)

//val crearTablaSIS_PERFIL = ("create table if not exists "
//        + " SIS_PERFIL (idPerfil integer primary key, "
//        + " nombre NVARCHAR(50) not null, descripcion NVARCHAR(100) null);")