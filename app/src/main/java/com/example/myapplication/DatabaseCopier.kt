package com.example.myapplication

import android.content.Context
import android.util.Log
import androidx.annotation.VisibleForTesting
import androidx.room.Room
import java.io.FileOutputStream
import java.io.IOException


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DatabaseCopier(context: Context) {

    private val TAG = DatabaseCopier::class.java.simpleName
    @VisibleForTesting
    private val DATABASE_NAME = "Atenea.db"

    private lateinit var mAppDataBase: AppDatabase
    private var appContext: Context? = null

    init {
        appContext = context
    }

    fun createDataBasefromAssest() {
        //call method that check if database not exists and copy prepopulated file from assets

        copyAttachedDatabase(appContext!!)
        mAppDataBase = Room.databaseBuilder(appContext!!, AppDatabase::class.java, DATABASE_NAME)
//            .fallbackToDestructiveMigration()
            .addMigrations(AppDatabase.MIGRATION_1_2)
            .fallbackToDestructiveMigration()
            .build()

//        mAppDataBase = RoomAsset.databaseBuilder(appContext!!, AppDatabase::class.java, DATABASE_NAME)
////            .addMigrations(AppDatabase.MIGRATION_1_2)
//            .allowMainThreadQueries()
//            .build()

    }

    fun getRoomDatabase(): AppDatabase {
        return mAppDataBase
    }

    private fun copyAttachedDatabase(context: Context) {
        val dbPath = context.getDatabasePath(DATABASE_NAME)

        // If the database already exists, return
        if (dbPath.exists()) {
            return
        }

        // Make sure we have a path to the file
        dbPath.parentFile.mkdirs()

        // Try to copy database file
        try {
            val inputStream = context.assets.open(DATABASE_NAME)
            val output = FileOutputStream(dbPath)
            val buffer = ByteArray(8192)

//            while ((inputStream.read(buffer, 0, 8192) ) > 0) {
            output.write(buffer, 0, inputStream.read(buffer, 0, 8192))
//            }

            output.flush()
            output.close()
            inputStream.close()
        } catch (e: IOException) {
            Log.d(TAG, "Failed to open file", e)
            e.printStackTrace()
        }

    }
}