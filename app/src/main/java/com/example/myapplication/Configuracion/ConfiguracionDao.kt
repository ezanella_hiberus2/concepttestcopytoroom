package com.example.myapplication.Configuracion

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ConfiguracionDao {

    /**
     * Test for me.
     */

    @Insert
    fun insertAll(configuracion: Configuracion)

//    @Query("SELECT * FROM SIS_CONFIGURACION")
//    fun getConfiguration() : List<Configuracion>


    /*******************************************************************************************************************
     * *************************************************************************************************************** *
     * Query from DAL                                                                                                  *
     * *************************************************************************************************************** *
     ******************************************************************************************************************/

    /**
     * Get the "ObtenerConfiguracion" for save the constants.
     */
    @Query("Select  articulos, idTerminal,vehiculo, intervalo, IDTPV, direccionBluetooth, direccionServicioWeb, actualizacion FROM SIS_CONFIGURACION")
    fun getConfigurationInitial(): Configuracion


    /**
     * "ObtenerConfiguracionBackUp"
     */
    @Query("Select articulos, idTerminal,vehiculo, intervalo, IDTPV, direccionBluetooth, direccionServicioWeb, actualizacion, tipoImpresora, margenImpresion, segundosImpresion, longitudLineaGrande, longitudLineaPequena FROM SIS_CONFIGURACION")
    fun getConfigurationBackUp(): Configuracion

    /**
     * "insertarConfiguracion"
     */
//    @Query ("INSERT INTO SIS_CONFIGURACION (idConfiguracion, articulos, idTerminal, vehiculo, kmVehiculo, actualizacion, intervalo, IDTPV, direccionServicioWeb, urlUpdates, margenImpresion, segundosImpresion, longitudLineaGrande, longitudLineaPequena, tipoImpresora)  VALUES (1, 10, '1', configuracion.vehiculo, configuracion.kmVehiculo, configuracion.actualizacion, '01' , configuracion.direccionServicioWeb, 'http://atenea-updates.hiberus.com', configuracion.margenImpresion, configuracion.segundosImpresion, configuracion.LongitudLineaGrande, configuracion.LongitudLineaPeq,  configuracion.tipoImpresora)")
//    fun insertConfiguration(configuracion: Configuracion)

    /**
     * Delete all the configuration
     */
    @Query("DELETE FROM SIS_CONFIGURACION WHERE idConfiguracion = 1")
    fun deleteConfiguration()

}