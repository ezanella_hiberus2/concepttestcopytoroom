package com.example.myapplication.Configuracion

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "SIS_CONFIGURACION")
data class Configuracion(
    @PrimaryKey
    @ColumnInfo(name = "idConfiguracion") val idConfiguracion: Int,
    val articulos: Int,
    val idTerminal: String,
    val vehiculo: String,
    val kmVehiculo: Float,
    val actualizacion: Boolean,
    val intervalo: Int,
    val IDTPV: String,
    val direccionBluetooth: String,
    val direccionServicioWeb: String,
    val urlUpdates: String,
    val margenImpresion: Int,
    val segundosImpresion: Int,
    val longitudLineaGrande: Int,
    val longitudLineaPequena: Int,
    val tipoImpresora: Int
)